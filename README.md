A starter of react web full-stack project.

TECH STACK:
- React
- Redux
- typescript
- tslint
- SASS
- Express
- Webpack



Scripts:
- To start on dev mode: `yarn run dev`;
- To start on production mode: `yarn run start` or `yarn run start_windows`;
- To use tslint:`yarn run lint`;
- To use prettier:`yarn run format`;
