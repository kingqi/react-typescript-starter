import * as bodyParser from 'body-parser'
import * as express from 'express'
import * as morgan from 'morgan'

import * as path from 'path'

import { webpackMiddleware } from './middleware/webpack'

const app = express()
webpackMiddleware(app)

// set up our express application
app.use(express.static(path.join(__dirname, '../public')))
app.use(morgan('dev')) // log every request to the console
app.use(bodyParser.json()) // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }))

// launch
const port = process.env.PORT || 3000
const server = app.listen(port, (err: string) => {
  if (err) {
    console.log(err)
  }
  console.log(
    `⚡ Express started on port ${port}, in ${process.env.NODE_ENV} mode`
  )
})

export { app }
