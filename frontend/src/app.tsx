import * as React from 'react'
import { Router, Route, Switch } from 'react-router-dom'

import { history } from './helpers/history'
import './global.scss'

class App extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <div>
            <Route exact path="/" component={() => <div>Hello world!</div>} />
          </div>
        </Switch>
      </Router>
    )
  }
}

export default App
