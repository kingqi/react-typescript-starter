import {
  createStore,
  applyMiddleware,
  StoreEnhancer
} from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { rootReducer, RootState } from '../reducers'


let middleware: StoreEnhancer
if (process.env.NODE_ENV === 'production') {
  middleware = applyMiddleware(thunkMiddleware)
} else {
  const loggerMiddleware = createLogger()
  middleware = applyMiddleware(thunkMiddleware, loggerMiddleware)
}

export const configureStore = function(initialState: RootState={}) {
  return createStore(rootReducer, initialState, middleware)
}
